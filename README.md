# Circus Marketing Frontend Test - Uziel Bueno

## Installing Dependencies

```bash
yarn install
# or
npm install
```

## Compiling SASS

```bash
yarn run build
# or
npm run build
```

## Serving

Open the `public/index.html` file in your browser to see the webpage.
